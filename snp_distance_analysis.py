######## Data science methodology using pandas ######## 
#%%
import pandas as pd

# Import data
#%%
ST239_all = pd.read_csv("ST239_all_distances.tsv", sep="\t")
ST239_all = ST239_all.rename(columns={"Unnamed: 0" : "id1"})
ST239_all.head()

# Convert data to tidy format: one observation per 
# %%
long_all_data = ST239_all.melt(id_vars="id1", var_name="id2", value_name="distance")
long_all_data.head()

#%%
### Analyse samples from outbreak ###
# List of putative outbreak sample ids
outbreak_sample_ids = ["G18252289", "G18252290", "G18252292", "G18252293", "G18252295", "G18252301", "G18252302", "G18252307", "G18252310", "G18255031", "G18255032", "G18255033", "G18255038", "G18255051", "G18255054", "G18255056", "G18255062", "G18255066", "G18255072", "G18255074", "G18255986", "G18255987", "G18255997", "G18256002", "G18256009", "G18256011", "G18256012", "G18256013", "G18256015", "G18256017", "G18256023", "G18256024", "G18256030", "G18256031", "G18256041", "G18256231", "G18256234", "G18256008", "G18256010", "G18256014", "G20250362", "G18255836"]

# Filter distances to only get those which are between samples which are both in the outbreak
dists_between_outbreak = long_all_data.query("id1 in @outbreak_sample_ids and id2 in @outbreak_sample_ids")
dists_between_outbreak.head()
dists_between_outbreak['distance'].describe()
#%% plot distribution
import seaborn as sns
import matplotlib.pyplot as plt

plt.figure(figsize = (15,8))
plt.xticks(rotation=90)
ax = sns.stripplot(x="id1", y="distance",data=dists_between_outbreak, linewidth=1)

# There's some weird going on with samples "G18256008", "G18256010", "G18256014", "G20250362" and "G18255836". Perhaps recombination wasn't removed??
#%% 
print(f"mean: {dists_between_outbreak['distance'].mean()}\nmin: {dists_between_outbreak['distance'].min()}\nmax: {dists_between_outbreak['distance'].max()}")

# %%
dists_between_outbreak_and_non_outbreak = long_all_data.query("(id1 in @outbreak_sample_ids and id2 not in @outbreak_sample_ids) or (id1 not in @outbreak_sample_ids and id2 in @outbreak_sample_ids)")
dists_between_outbreak_and_non_outbreak.head()
dists_between_outbreak_and_non_outbreak['distance'].describe()
# dists_between_outbreak_and_non_outbreak.query("id == 'G20250811'")

#%%
print(f"mean: {dists_between_outbreak_and_non_outbreak['distance'].mean()}\nmin: {dists_between_outbreak_and_non_outbreak['distance'].min()}\nmax: {dists_between_outbreak_and_non_outbreak['distance'].max()}")

######## Python for loop and data structure methodology ######## 

# data structure will look like
# {
#     "G20250811": {
#         "G20250813": 0,
#         "G18255031": 136,
#         "G18252293": 135
#     },
#     "G20250813": {
#         "G20250811": 0,
#         "G18255031": 136,
#         "G18252293": 135  
#     }
# }
# %%
import csv

# make a dictionary to hold the data
distances = {}

with open("ST239_all_distances.tsv") as f:
    reader = csv.DictReader(f, delimiter = "\t")
    # loop through the rows
    for row in reader:
        # the id column header is blank so to get id access via row['']
        sample_id = row['']
        # make a diction for the new sample
        distances[sample_id] = {}

        # get the rest of the row headers which have other samples
        sample_ids = list(row.keys())
        # remove id field
        sample_ids.remove('')
        # get distances
        for other_sample_id in sample_ids:
            if sample_id != other_sample_id:
                # set the distance for sample_id vs other_sample_id
                distances[sample_id][other_sample_id] = int(row[other_sample_id])
# %%
# get distances for outbreak samples
import numpy
outbreak_distances = []
for outbreak_sample_id in outbreak_sample_ids:
    # if distance is between two outbreak samples add to outbreak_distances list
    for other_sample_id in distances[outbreak_sample_id]:
        if other_sample_id in outbreak_sample_ids:
            outbreak_distances.append( distances[outbreak_sample_id][other_sample_id])
print(f"mean: {numpy.mean(outbreak_distances)}\nmin: {numpy.min(outbreak_distances)}\nmax: {numpy.max(outbreak_distances)}")
# %%
# get distances between outbreak samples and non-outbreak samples
import numpy
outbreak_vs_non_outbreak_distances = []
for outbreak_sample_id in outbreak_sample_ids:
    # if distance is between an outbreak and non-outbreak sample add to outbreak_distances list
    for other_sample_id in distances[outbreak_sample_id]:
        if other_sample_id not in outbreak_sample_ids:
            outbreak_vs_non_outbreak_distances.append( distances[outbreak_sample_id][other_sample_id])
print(f"mean: {numpy.mean(outbreak_vs_non_outbreak_distances)}\nmin: {numpy.min(outbreak_vs_non_outbreak_distances)}\nmax: {numpy.max(outbreak_vs_non_outbreak_distances)}")
