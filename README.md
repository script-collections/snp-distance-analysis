# SNP distance assessment

The exercise starts with a SNP distance matrix from all samples of ST239 from _Staphylococcus aureus_ from the India GHRU collection.

![CC239](images/CC239_tree.jpg)
**A phylogeny of ST239 and SLVs** [Microreact link](https://microreact.org/project/bggsvS2EwC1ZTXjfANCsMz-st239-phylogeny#djln-cc239)

![CC239](images/ST239_tree.jpg)
**The same phylogeny with 74 ST239 samples selected** [Microreact link](https://microreact.org/project/bggsvS2EwC1ZTXjfANCsMz-st239-phylogeny#samc-st239)

The hypothesis is that there are 37 isolates all from one place that are part of an outbreak.

![CC239](images/putative_outbreak.jpg)
**The same phylogeny with 37 putative outbreak samples  selected** [Microreact link](https://microreact.org/project/bggsvS2EwC1ZTXjfANCsMz-st239-phylogeny#q7ky-putative-outbreak)

To help test this hypothesis we will look at the following two questions:

1. What is the distribution of SNPs amongst the putative outbreak samples
2. What is the distance to samples from the 37 putative outbreak samples and all other ST239 samples.

How do these distributions compare?

We will use 2 different approaches.

* A data science method using pandas
* A more traditional method constructing a data structure with a for loop

See the [snp_distance_analysis.py](snp_distance_analysis.py)

You can run this online at [mybinder.org/v2/gl/script-collections%2Fsnp-distance-analysis/HEAD?labpath=snp_distance_analysis.py](https://mybinder.org/v2/gl/script-collections%2Fsnp-distance-analysis/HEAD?labpath=snp_distance_analysis.py)

See this video on how to execute the code on mybinder:

[![Screenshot](https://img.youtube.com/vi/KME87xIeW0E/0.jpg)](https://youtu.be/KME87xIeW0E)